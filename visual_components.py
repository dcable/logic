import pyglet
from pyglet import gl
from pyglet.window import key

import components as c
from geometry import Rect, Point, Circle    


class VisualComponent(object):
    """Abstract visual wrapper for all components.
    1 input -> 1 output.
    Do no instantiate directly."""
    
    def __init__(self, tex=None, alt=None):
        self.gate = None
        #self.gate.sprite = self
        self.set_texture(tex)
        self.set_alt_texture(alt)
        
        # i/o nodes
        self.i_nodes = {}
        self.o_nodes = {}

        # node position offsets
        self.i_offset_x = -28
        self.i_offset_y = 0
        self.o_offset_x = 28
        self.o_offset_y = 0
        
        # pos
        self.x = None
        self.y = None
        self.rect = None
        
        # Default 1 input, 1 output
        self.i_nodes[0] = Node(parent=self, node_type='i')
        self.o_nodes[0] = Node(parent=self, node_type='o')
        
    def draw(self, texture, selected=False):
        gl.glEnable(gl.GL_BLEND)
        if self.get_state():
            # Color: red
            r,g,b,a = 1.0, 0.5, 0.5, 1.0
        else:
            # Color: black
            r,g,b,a = 0.0, 0.0, 0.0, 1.0
        if selected:
            # (add blue tint)
            b = 1.0
        gl.glColor4f(r,g,b,a)
            
            
        texture.blit(self.x, self.y)
        gl.glDisable(gl.GL_BLEND)
        
    def hits_node(self, point):
        """Return first node where point falls within hotspot circle.
        There is no special handling for overlapping hotspots."""
        # try input nodes
        if self.i_nodes:
            for key in self.i_nodes.keys():
                node = self.i_nodes[key]
                if node.has_point(point):
                    return node
                
        # try output nodes
        if self.o_nodes:
            for key in self.o_nodes.keys():
                node = self.o_nodes[key]
                if node.has_point(point):
                    return node
                
        return None
        

    def set_component(self, comp):
        self.gate = comp()
        self.gate.sprite = self
    
    def set_texture(self, texture):
        self.texture = texture
        
    def set_size(self, w, h):
        self.width = w
        self.height = h
            
    def set_alt_texture(self, texture):
        self.alt_texture = texture
        
        
    def set_pos(self, x, y):
        self.x = x
        self.y = y
        
        # calculate boundary rect
        p1 = Point(self.x - (self.width//2), self.y - (self.height//2))
        p2 = Point(self.x + (self.width//2), self.y + (self.height//2))
        self.rect = Rect(p1,p2)
        
        # i/o nodes
        if self.i_nodes:
            self.i_nodes[0].set_pos(x + self.i_offset_x, y - self.i_offset_y + 1)
        if self.o_nodes:
            self.o_nodes[0].set_pos(x + self.o_offset_x, y + self.o_offset_y + 1)
            
    # Wrappers
    def has_point(self, point):
        return self.rect.has_point(point)
    
    def connect(self, sprite, index=0):
        # Wrapped by Node.connect
        self.gate.connect(sprite.gate, index)
        
    def disconnect(self, sprite):
        self.gate.disconnect(sprite.gate)
        
    def unwire(self):
        self.gate.unwire()
        
    def get_type(self):
        return self.gate.comp
        
    def get_state(self):
        return self.gate.get_state()
        
    def get_update_flag(self):
        return self.gate._update_me

    def is_end(self):
        return self.gate.is_end()
        
    def set_update_flag(self, flag=True):
        self.gate._update_me = flag
        
    def update_state(self, check_flag=True):
        self.gate.chain_op(check_flag)
        
        
class LogicGate(VisualComponent):
    """Abstract Gate
    2 input -> 1 output.
    Do not instantiate directly."""
    def __init__(self, tex, alt):
        VisualComponent.__init__(self, tex, alt)
        # node position offsets
        self.i_offset_y = 6
        
        # add second input node
        self.i_nodes[1] = Node(parent=self, node_type='i', node_index=1)
        
    def set_pos(self, x, y):
        VisualComponent.set_pos(self, x, y)
        # position second input node
        self.i_nodes[1].set_pos(x + self.i_offset_x, y + self.i_offset_y + 1)
        
class Switch(VisualComponent):
    def __init__(self, tex, alt):
        VisualComponent.__init__(self, tex, alt)
        self.set_component(c.Switch)
        
    def is_closed(self):
        return self.gate.is_closed()
    
    def switch(self):
        self.gate.switch()
        # switch texture
        tex = self.texture
        self.texture = self.alt_texture
        self.alt_texture = tex
        
class Source(VisualComponent):
    def __init__(self, tex, alt):
        VisualComponent.__init__(self, tex, alt)
        self.set_component(c.Source)
        
        # no input
        self.i_nodes = None
        
class Indicator(VisualComponent):
    def __init__(self, tex, alt):
        VisualComponent.__init__(self, tex, alt)
        self.set_component(c.Indicator)
        
        # no output
        self.o_nodes = None
        
class NotGate(VisualComponent):
    def __init__(self, tex, alt):
        VisualComponent.__init__(self, tex, alt)
        self.set_component(c.Not)
    
class OrGate(LogicGate):
    def __init__(self, tex, alt):
        LogicGate.__init__(self, tex, alt)
        self.set_component(c.Or)
        
class NorGate(LogicGate):
    def __init__(self, tex, alt):
        LogicGate.__init__(self, tex, alt)
        self.set_component(c.Nor)
        
class XorGate(LogicGate):
    def __init__(self, tex, alt):
        LogicGate.__init__(self, tex, alt)
        self.set_component(c.Xor)

class XnorGate(LogicGate):
    def __init__(self, tex, alt):
        LogicGate.__init__(self, tex, alt)
        self.set_component(c.Xnor)
        
class AndGate(LogicGate):
    def __init__(self, tex, alt):
        LogicGate.__init__(self, tex, alt)
        self.set_component(c.And)
        
class NandGate(LogicGate):
    def __init__(self, tex, alt):
        LogicGate.__init__(self, tex, alt)
        self.set_component(c.Nand)


class Node(object):
    """Connection node for wiring"""
    
    # radius of i/o node hotspots
    click_radius = 7
    
    def __init__(self, parent, node_type, node_index=0, x=None, y=None):
        if node_type not in ('i', 'o'):
            raise ValueError
        
        self.parent = parent
        self.type = node_type
        self.index = node_index
        self.x = x
        self.y = y
        
        if x and y:
            self.set_pos(x,y)
        else:
            self.hotspot = None
            
    def connect(self, node):
        # Validate connection:
        #     - disallow in<->in, out<->out
        #     - disallow wiring a gate to itself!
        if self.parent is node.parent:
            # TODO: raise/handle InvalidConnectionError
            print('[Nope] Tried to wire a gate to itself.')
        elif self.type == node.type:
            # TODO: raise/handle InvalidConnectionError
            print('[Nope] Attempted i-i or o-o connection.')
        else:
            # TODO: handle components.CircularConnectionError
            if self.type == 'i':
                self.parent.connect(node.parent, self.index)
            else:
                node.parent.connect(self.parent, node.index)
            #print('[Connection OK]')
                

    def has_point(self, point):
        return self.hotspot.has_point(point)
        
    def set_pos(self, x, y):
        self.x = x
        self.y = y
        self.hotspot = Circle(Point(self.x, self.y), self.click_radius)
