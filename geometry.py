import math

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self._current = 0
    
    def __iter__(self):
        return self
        
    def next(self):
        # [x,y]
        self._current += 1
        
        if self._current == 1:
            return self.x
        elif self._current == 2:
            return self.y
        else:
            self._current = 0
            raise StopIteration
            

class Rect(object):
    def __init__(self, p1, p2):
        # untangle (force p1=lower-left, p2=upper-right)
        x_list = sorted([p1.x, p2.x])
        y_list = sorted([p1.y, p2.y])
        
        self.p1 = Point(x_list[0], y_list[0])
        self.p2 = Point(x_list[1], y_list[1])
        self._current = 0

    def __iter__(self):
        return self
        
    def next(self):
        # [p1,p2]
        self._current += 1
        
        if self._current == 1:
            return self.p1
        elif self._current == 2:
            return self.p2
        else:
            self._current = 0
            raise StopIteration
        
    def has_point(self, point):
        """Return True if rect contains point."""
        x,y = point
        x1,y1 = self.p1
        x2,y2 = self.p2

        if x < x1 or x > x2:
            return False
        if y < y1 or y > y2:
            return False
        
        return True
        
    def intersects(self, rect):
        """Return True if rects A and B overlap."""
        # Check if 4 corners of B intersect A, then check A against B.
        # Can this be done more efficiently?
        # TODO: research collision detection.
        for p in [self.p1,
                   self.p2,
                   Point(self.p1.x, self.p2.y),
                   Point(self.p2.x, self.p1.y)
        ]:
            if rect.has_point(p):
                return True
                
        for p in [rect.p1,
                   rect.p2,
                   Point(rect.p1.x, rect.p2.y),
                   Point(rect.p2.x, rect.p1.y)
        ]:
            if self.has_point(p):
                return True
        
        return False

class Circle(object):
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius
        
    def has_point(self, point):
        a = self.center.x - point.x
        b = self.center.y - point.y
        c = math.sqrt(a**2 + b**2)
        
        if c <= self.radius:
            return True
        return False
