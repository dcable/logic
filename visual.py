import os
import pickle
import ConfigParser

import pyglet
from pyglet import gl
from pyglet.window import key

from components import CircularConnectionError
import visual_components as vc
import geometry as g

        
class Editor(pyglet.window.Window):
    # TODO: Reorganize mouse click handling.
    
    def __init__(self):
        pyglet.window.Window.__init__(self)
        
        # defaults (should be overwritten by settings.cfg)
        self.width = 800
        self.height = 600
        self.branch_point_thresh = 2
        self.drag_thresh = 3
        self.line_smooth = False
        self.texture_path = 'images'
        
        # component data
        # comp_name: (comp_class, alt_texuture)
        self.comp_data = {
            'not': (vc.NotGate, None), 
            'or': (vc.OrGate, None), 
            'nor': (vc.NorGate, None),
            'xor': (vc.XorGate, None),
            'xnor': (vc.XnorGate, None),
            'and': (vc.AndGate, None),
            'nand': (vc.NandGate, None),
            'source': (vc.Source, None),
            'switch': (vc.Switch, 'switch_closed'),
            'indicator': (vc.Indicator, None),
        }

        # component selection order
        self.comp_order = ['not', 'or', 'nor', 'xor', 'xnor', 'and', 'nand',
                      'source', 'switch', 'indicator']
        
        self.components = []
        self.textures = None

        # mouse pos
        self.mouse_x = 0
        self.mouse_y = 0
        
        # object manipulation data
        self.drag_items = None
        self.drag_offset_x = None
        self.drag_offset_y = None
        self.drag_start_x = None
        self.drag_start_y = None
        self.selecting = False
        self.select_items = None
        
        # flag indicates when an action was taken on previous 
        # mouse_press event, so mouse_release can be ignored
        self.operating = False
        
        # wiring mode data
        self.wiring_from = None
        self.wiring_node = None
        
        self.load_config('settings.cfg')
        self.load_textures(path=self.texture_path)
        self.set_gl_params()
        
        # (selected index of self.comp_order)
        self.selected_comp = 0
        
        pyglet.app.run()
        
    # Initialization methods
    def set_gl_params(self):
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        #gl.glEnable(gl.GL_BLEND)
        #gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_TEXTURE_2D)
        if self.line_smooth:
            gl.glEnable(gl.GL_LINE_SMOOTH)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA) 
        
        # set background color
        r,g,b,a = (0.5, 0.5, 0.5, 1.0)
        gl.glClearColor(r,g,b,a)
        
    def load_config(self, config_file):
        if not os.path.isfile(config_file):
            print('[Config file not found - using defaults]')
            return
        config = ConfigParser.RawConfigParser()
        config.read(config_file)
        
        self.width = config.getint('display', 'screen_width')
        self.height = config.getint('display', 'screen_height')
        self.branch_point_thresh = config.getint('display', 
            'branch_indicator_threshold')
        self.line_smooth = config.getboolean('display', 'line_smooth')
        self.drag_thresh = config.getint('mouse', 'drag_threshold')
        self.texture_path = config.get('resources', 'component_textures')
        
    def load_textures(self, path):
        """Load textures from image files"""
        # TODO: decide IOError handling
        self.textures = {}
        
        keys = self.comp_data.keys()
        for key in self.comp_data.keys():
            comp, alt_tex = self.comp_data[key]
            if alt_tex:
                keys.append(alt_tex)
        for comp in keys:
            filename = comp + '.png'
            full_path = '/'.join([path, filename])
            
            image = pyglet.resource.image(full_path)
            texture = image.get_texture()
            # scale it
            w = image.width * 2
            h = image.height * 2
            texture.width = w
            texture.height = h
            texture.anchor_x = w // 2
            texture.anchor_y = h // 2
            
            self.textures[comp] = texture
            
    # Editor actions / functions
    def create_selected_comp(self, x, y):
        name = self.comp_order[self.selected_comp]
        comp, alt_tex = self.comp_data[name]
        
        gate = comp(name, alt_tex)

        tex = self.textures[name]
        gate.set_size(tex.width, tex.height)
        gate.set_pos(x,y)
        self.components.append(gate)
        self.update_states()
        
    def delete_comp(self, comp):
        comp.unwire()
        if comp in self.components:
            self.components.remove(comp)
        self.update_states()
        
    def get_clicked_comp(self, x, y):
        """Return (comp, node) at point x,y"""
        point = g.Point(x,y)
        
        for gate in self.components:
            if gate.has_point(point):
                node = gate.hits_node(point)
                return (gate, node)
        return (None, None)
        
    def load_from_file(self):
        """Quick-load component layout from file."""
        try:
            with open('save.pkl', 'r') as f:
                self.components = pickle.load(f)
            print('[Load OK]')
        except IOError:
            print('[Load failed - no save file found]')
    
    def save_to_file(self):
        """Quick-save component layout to file.
        This breaks if we store ctypes pointers within the component 
        class, such as pyglet textures and images."""
        # TODO: file name / save slot
        with open('save.pkl', 'w') as f:
            pickle.dump(self.components, f)
        print('[Save OK]')
        
    def select_comp(self, index):
        if index < 0 or index >= len(self.comp_order):
            raise IndexError
        self.selected_comp = index
        
    def select_comp_by_name(self, name):
        self.selected_comp = self.comp_order.index(name)
            
    def update_states(self):
        """Update state for all components."""
        for gate in self.components:
            gate.set_update_flag()
        for gate in self.components:
            if gate.is_end():
                gate.update_state()

    # Drawing methods
    def draw_selected(self):
        """Indicate selected gate in lower-left corner."""
        # Color: dark yellow
        r,g,b,a = (0.7, 0.7, 0.0, 1.0)
        gl.glColor4f(r,g,b,a)
        gl.glEnable(gl.GL_BLEND)
        name = self.comp_order[self.selected_comp]
        tex = self.textures[name]
        tex.blit(tex.width//2, tex.height//2 + 12)
        label = pyglet.text.Label(name.upper(),
            font_size=10,
            x=5, y=2)
        label.draw()
        gl.glDisable(gl.GL_BLEND)
        
    def draw_line(self, x1, y1, x2, y2, rgba):
        """Direct line between (x1,y1) and (x2,y2)"""
        r,g,b,a = rgba
        gl.glColor4f(r,g,b,a)
        pyglet.graphics.draw(2, gl.GL_LINES, ('v2i', (x1,y1, x2,y2)))
        
    def draw_square(self, x, y, width, rgba):
        """Filled square with center at (x,y)"""
        r,g,b,a = rgba
        o = width // 2
        gl.glColor4f(r,g,b,a)
        verts = (x-o, y-o, x+o, y-o, x+o, y+o, x-o, y+o)
        pyglet.graphics.draw(4, gl.GL_QUADS, ('v2i', verts))
        
    def draw_select_rect(self):
        """Draw selection rectangle from start to current mouse pos"""
        # Color: light blue
        r,g,b,a = (0.4, 0.9, 0.9, 0.5)
        gl.glColor4f(r,g,b,a)
        x1 = self.mouse_x
        y1 = self.mouse_y
        x2 = self.drag_start_x
        y2 = self.drag_start_y
        pyglet.graphics.draw(2, gl.GL_LINES, ('v2i', (x1,y1, x1,y2)))
        pyglet.graphics.draw(2, gl.GL_LINES, ('v2i', (x1,y2, x2,y2)))
        pyglet.graphics.draw(2, gl.GL_LINES, ('v2i', (x2,y2, x2,y1)))
        pyglet.graphics.draw(2, gl.GL_LINES, ('v2i', (x2,y1, x1,y1)))
        
    def draw_connection(self, x1, y1, x2, y2, rgba):
        """Connect points (x1,y1) (x2,y2) without using sloped lines"""
        mid_x = (x2 + x1) // 2
        self.draw_line(x1, y1, mid_x, y1, rgba)
        self.draw_line(mid_x, y1, mid_x, y2, rgba)
        self.draw_line(mid_x, y2, x2, y2, rgba)
        # Draw "branch point" unless line is horizontal.
        if abs(y2 - y1) > self.branch_point_thresh:
            self.draw_square(mid_x, y2, 6, rgba)
            
    # Event handlers
    def on_draw(self):
        self.clear()
        self.draw_selected()

        # draw gates, existing connections
        for gate in self.components:
            # TODO: this could be handled more efficiently
            selected = self.select_items and gate in self.select_items
            tex = self.textures[gate.texture]
            gate.draw(tex, selected)
            
            if not gate.gate.inputs:
                continue
                
            for i in range(len(gate.gate.inputs)):
                if gate.gate.inputs[i]:
                    # TODO: Include connection drawing in component?
                    gate2 = gate.gate.inputs[i].sprite
                    x1 = gate.i_nodes[i].x
                    y1 = gate.i_nodes[i].y
                    x2 = gate2.o_nodes[0].x
                    y2 = gate2.o_nodes[0].y
                    
                    # Color: white
                    rgba = (1.0, 1.0, 1.0, 1.0)
                    self.draw_connection(x1, y1, x2, y2, rgba)
        
        # If we're creating a connection, pull wire from node to cursor.
        if self.wiring_from:
            gate = self.wiring_from
            node = self.wiring_node
            
            # Color: light green
            rgba = (0.7, 1.0, 0.7, 0.5)
            self.draw_line(self.mouse_x, self.mouse_y, node.x, node.y,
                rgba)
                
        if self.selecting:
            self.draw_select_rect()
            
    def on_key_press(self, symbol, modifiers):
        # Gate selection
        if symbol == key._1:
            self.select_comp(0)
        if symbol == key._2:
            self.select_comp(1)
        if symbol == key._3:
            self.select_comp(2)
        if symbol == key._4:
            self.select_comp(3)
        if symbol == key._5:
            self.select_comp(4)
        if symbol == key._6:
            self.select_comp(5)
        if symbol == key._7:
            self.select_comp(6)
            
        # Component selection
        if symbol == key.Q:
            self.select_comp(7)
        if symbol == key.W:
            self.select_comp(8)
        if symbol == key.E:
            self.select_comp(9)
            
        # Commands
        if symbol == key.DELETE:
            if self.select_items:
                for comp in self.select_items:
                    self.delete_comp(comp)
                self.select_items = None
            
        if symbol == key.F5:
            self.save_to_file()
        if symbol == key.F7:
            self.load_from_file()
            
            
    def on_mouse_motion(self, x, y, dx, dy):
        self.mouse_x = x
        self.mouse_y = y
            
    def on_mouse_press(self, x, y, button, modifiers):
        # middle click
        if button == 2:
            self.selecting = False
            return
            
        self.drag_start_x = x
        self.drag_start_y = y
        gate, node = self.get_clicked_comp(x,y)
        
        # right-click
        if button == 4:
            # click on nothing, wiring in progress: drop the wire
            if not gate and self.wiring_from:
                self.wiring_from = None
                self.wiring_node = None
            # click on gate, not wiring: delete the gate
            elif gate and not self.wiring_from:
                self.delete_comp(gate)
            return
        
        elif button == 1:
            # click on nothing: begin selection
            if not gate:
                self.selecting = True
            
            # click on gate
            else:
                # SHIFT held: operate component (if possible)
                if modifiers & key.MOD_SHIFT:
                    self.operating = True
                    if gate.get_type() == 'switch':
                        gate.switch()
                        self.update_states()
                else:
                    self.handle_gate_click(gate, node, modifiers)
                    
    def handle_gate_click(self, gate, node, modifiers):
        """Handle left-click within gate.rect"""
        # click on node, not wiring: create new wire
        if node and not self.wiring_from:
            # start new wire
            self.wiring_from = gate
            self.wiring_node = node
            self.operating = True
        # click on node, wiring in progress: try to connect the wire
        elif node:
            try:
                node.connect(self.wiring_node)
            except CircularConnectionError:
                # TODO: error message for the user
                pass
            self.wiring_from = None
            self.wiring_node = None
            self.update_states()
            self.operating = True
        # click was not on a node...
        else:
            # CTRL held: select / de-select component
            if modifiers & key.MOD_CTRL:
                if not self.select_items:
                    self.select_items = []
                if gate not in self.select_items:
                    self.select_items.append(gate)
                else:
                    self.select_items.remove(gate)
                return
            # comp not in selection group: clear selection, drag comp
            if not self.select_items or gate not in self.select_items:
                self.select_items = None
                # Drag the gate.
                # use offset to prevent gate jumping to mouse pos
                off_x = gate.x - self.mouse_x
                off_y = gate.y - self.mouse_y
                self.drag_items = [(gate, off_x, off_y)]
            # comp in selection group: drag entire group
            else:
                if not self.drag_items:
                    self.drag_items = []
                    for comp in self.select_items:
                        off_x = comp.x - self.mouse_x
                        off_y = comp.y - self.mouse_y
                        self.drag_items.append((comp, off_x, off_y))
                
    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        self.mouse_x = x
        self.mouse_y = y
        
        if self.drag_items:
            for item, off_x, off_y in self.drag_items:
                item.set_pos(x + off_x, y + off_y)
                
    def on_mouse_release(self, x, y, button, modifiers):
        if button == 1:
            # Do nothing if an action was taken on mouse_press.
            if self.operating:
                self.operating = False
                return
            
            gate, node = self.get_clicked_comp(x,y)
            dx = abs(self.drag_start_x - x)
            dy = abs(self.drag_start_y - y)
            
            # click or drag?
            click = dx < self.drag_thresh and dy < self.drag_thresh
            
            # box select finished: select all components inside rect
            if self.selecting and not click:
                self.select_items = []
                p1 = g.Point(self.drag_start_x, self.drag_start_y)
                p2 = g.Point(x, y)
                rect = g.Rect(p1, p2)
                for gate in self.components:
                    if rect.intersects(gate.rect):
                        self.select_items.append(gate)
            # click on nothing, non-empty selection: clear selection
            elif not gate and self.select_items:
                self.select_items = None
            # click on nothing, empty selection: create selected comp
            elif not gate and not self.drag_items:
                if click:
                    self.create_selected_comp(x,y)
            # click on gate: select it
            elif gate:
                if click and not (modifiers & key.MOD_CTRL):
                    self.select_items = [gate]
                    
            self.drag_items = None
            self.drag_offset_x = None
            self.drag_offset_y = None
            self.drag_start_x = None
            self.drag_start_y = None
            self.selecting = False
            
    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        """Cycle component selection"""
        self.selected_comp += scroll_y
        if self.selected_comp >= len(self.comp_order):
            self.selected_comp = 0
        elif self.selected_comp < 0:
            self.selected_comp = len(self.comp_order) - 1
        
       
if __name__ == '__main__':
    e = Editor()
