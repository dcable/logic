import unittest
import components as c


class IndicatorTest(unittest.TestCase):
    def setUp(self):
        self.source1 = c.Source()
        self.ind = c.Indicator()
        
        self.ind.connect(self.source1)
        
    def test_on(self):
        self.source1.enable()
        self.ind._op()
        
        assert self.ind.get_state() == True
        
    def test_off(self):
        self.source1.disable()
        self.ind._op()
        
        assert self.ind.get_state() == False
        
class SwitchTest(unittest.TestCase):
    def setUp(self):
        self.source1 = c.Source()
        self.switch = c.Switch()
        self.ind = c.Indicator()
        
        self.switch.connect(self.source1)
        self.ind.connect(self.switch)
        self.source1.enable()
        
    def test_open(self):
        self.switch.open()
        self.ind.chain_op()
        
        assert self.switch.get_state() == False
        assert self.ind.get_state() == False
        
    def test_closed(self):
        self.switch.close()
        self.ind.chain_op()
        
        assert self.switch.get_state() == True
        assert self.ind.get_state() == True
        
class SplitTest(unittest.TestCase):
    def setUp(self):
        self.t = c.Testbed()
        self.source = c.Source()
        self.t.add_component(self.source, 'source')
        self.t.add_component(c.Split(), 'split')
        self.t.add_component(c.Indicator(), 'i1')
        self.t.add_component(c.Indicator(), 'i2')
        self.t.add_component(c.Indicator(), 'i3')
        self.t.wire('source', 'split')
        self.t.wire('split', 'i1')
        self.t.wire('split', 'i2')
        self.t.wire('split', 'i3')
        
    def test_on(self):
        self.source.enable()
        self.t.update_states()
        
        for key in ['i1', 'i2', 'i3']:
            assert self.t.components[key].get_state() == True
            
    def test_off(self):
        self.source.disable()
        self.t.update_states()
        
        for key in ['i1', 'i2', 'i3']:
            assert self.t.components[key].get_state() == False

if __name__ == '__main__':
    
    # Test cases to run
    cases = [IndicatorTest,
             SwitchTest,
             SplitTest,
            ]
    for case in cases:
        suite = unittest.TestLoader().loadTestsFromTestCase(case)
        unittest.TextTestRunner(verbosity=2).run(suite)
