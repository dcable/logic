##############
# EXCEPTIONS #
##############
class InvalidInputError(IndexError):
    # index of input node is invalid
    pass
    
class IncompleteUpdateError(Exception):
    # components were somhow missed during a complete state update
    pass
    
class CircularConnectionError(Exception):
    # connection was attempted which would create a loop
    pass
    
    
##############    
# COMPONENTS #
##############
class Component(object):
    """Base component behavior"""
    # component label
    comp = '(generic component)'
    
    def __init__(self):
        # Inputs are fixed, outputs can connect to any number of comps.
        self.inputs = [None]
        self.input_count = len(self.inputs)
        self.outputs = []
        self.state = False
        # Flag for resolving state
        self._update_me = False
        
        
    # Helpers
    def _set_state(self, state):
        self.state = state
        
    def _validate_input(self, i):
        """Raise exception if the index is invalid."""
        if i < 0 or i > (self.input_count-1):
            raise InvalidInputError

    def _test_input(self, i=0):
        """Determine state of input component (if any)"""
        self._validate_input(i)

        if self.inputs[i]:
            return self.inputs[i].get_state()
        return False
        
    def _get_test(self):
        """Return state list for connected inputs"""
        test = [False for i in range(self.input_count)]
        for i in range(len(self.inputs)):
            test[i] = self._test_input(i)
        return test

    def _op(self):
        """Set component state based on state of connected inputs."""
        if self.inputs:
            if self.inputs[0]:
                self._set_state(self.inputs[0].get_state())
            else:
                self._set_state(False)
            
    def chain_op(self, check_flag=False):
        """set state for entire input chain"""
        if check_flag and self._update_me == False:
            return
        
        if self.inputs:
            for input in self.inputs:
                if input:
                    input.chain_op()
        self._op()
        self._update_me = False
        

    # Connections
    def connect(self, comp, i=0):
        """Connect 'comp' output to input i."""
        self._validate_input(i)
        # remove existing connection on this input
        if self.inputs[i]:
            self.disconnect(self.inputs[i])
        
        self.inputs[i] = comp
        if self not in comp.outputs:
            comp.outputs.append(self)

            if self.detect_circular(self):
                self.disconnect(comp)
                raise CircularConnectionError
        
    def disconnect(self, comp=None):
        """Disconnect 'comp' from inputs.
        If None, disconnect all inputs."""
        if not comp:
            for input in self.inputs:
                self.disconnect(input)
        else:
            for i in range(len(self.inputs)):
                if self.inputs[i] is comp:
                    self.inputs[i] = None
            comp.outputs.remove(self)
        
    def unwire(self):
        """Remove all connections to/from self"""
        if self.inputs:
            for i in self.inputs:
                if i:
                    self.disconnect(i)
        if self.outputs:
            # Copy list since original will be modified by disconnects
            outputs = self.outputs[:]
            for o in outputs:
                o.disconnect(self)
                
                
    def detect_circular(self, root=None):
        if not self.outputs:
            return False
            
        if root in self.outputs:
            return True
        else:
            flags = [o.detect_circular(root) for o in self.outputs]
            return True in flags
        
        
    # Info
    def get_state(self):
        return self.state
        
    def fed_by(self, comp):
        """Is 'comp' connected directly to one of the inputs?"""
        for c in self.inputs:
            if c is comp:
                return True
        return False
        
    def feeds(self, comp):
        """Is 'comp' connected directly to one of the outputs?"""
        for c in self.outputs:
            if c is comp:
                return True
        return False
        
    def is_end(self):
        """Return True if no outputs are connected."""
        if not self.outputs:
            return True
        else:
            for c in self.outputs:
                if c:
                    return False
            return True
        
        
class Source(Component):
    """Starting point. 0->n"""
    comp = 'source'
    
    def __init__(self):
        Component.__init__(self)
        self.input_count = 0
        self.inputs = None
        self._set_state(True)
        
    def enable(self):
        self._set_state(True)
        
    def disable(self):
        self._set_state(False)
        


class Switch(Component):
    """Open or closed."""
    comp = 'switch'
    
    def __init__(self):
        Component.__init__(self)
        self.closed = False
        
    def switch(self):
        self.closed = not self.closed
        
    def open(self):
        self.closed = False
        
    def close(self):
        self.closed = True
        
    def is_closed(self):
        return self.closed
        
    def _op(self):
        # state = False if switch is open
        if not self.closed:
            self._set_state(False)
        else:
            Component._op(self)
            
class Split(Component):
    """Connection node.  1->n"""
    comp = 'split'
    
        
class Indicator(Component):
    """LED/Light (end point)"""
    comp = 'indicator'
    
    def __init__(self):
        Component.__init__(self)
        self.outputs = None
        
    
#########
# GATES #
#########
# (composite gates implemented directly for the sake of efficiency)
class Gate(Component):
    """Logic gate. 2->n"""
    comp='(generic gate)'
    
    def __init__(self):
        Component.__init__(self)
        self.inputs = [None, None]
        self.input_count = len(self.inputs)
        self.state = False
        
    def find_ends(self):
        # trace output, return list of end components
        if not self.outputs:
            return [self]
        ends = [o.find_ends() for o in self.outputs]
        # return flattened list
        return [item for sublist in ends for item in sublist]
        
        
class Not(Gate):
    comp = 'not'
    
    def _op(self):
        Gate._op(self)
        self._set_state(not self.state)
    
class And(Gate):
    comp = 'and'
    
    def _op(self):
        test = self._get_test()
        self._set_state(test[0] and test[1])
        
class Nand(And):
    comp = 'nand'
    
    def _op(self):
        And._op(self)
        self._set_state(not self.state)
            
class Or(Gate):
    comp = 'or'
    
    def _op(self):
        test = self._get_test()
        self._set_state(test[0] or test[1])
        
class Nor(Or):
    comp = 'nor'
    
    def _op(self):
        Or._op(self)
        self._set_state(not self.state)
            
class Xor(Gate):
    comp = 'xor'
    
    def _op(self):
        test = self._get_test()
        self._set_state(test[0] != test[1])
        
class Xnor(Xor):
    comp = 'xnor'
    
    def _op(self):
        Xor._op(self)
        self._set_state(not self.state)



#########
# TOOLS #
#########
class Testbed(object):
    """Test container with tools to keep states consistent"""
    def __init__(self):
        self.components = {}
        
    def add_component(self, comp, label):
        self.components[label] = comp
        
    def wire(self, c1, c2, node=0):
        """connect from output of c1 to input (i) of c2"""
        comp1 = self.components[c1]
        comp2 = self.components[c2]
        
        comp2.connect(comp1, node)
        
    def update_states(self):
        for comp in self.components.values():
            comp._update_me = True
        for comp in self.components.values():
            if comp.is_end():
                comp.chain_op(check_flag=True)
                
    def dump_states(self):
        for key in sorted(self.components.keys()):
            print('{0}: {1} ({2})'.format(
                key, 
                self.components[key].get_state(),
                self.components[key]._update_me)
            )


########
# MAIN #
########    
if __name__ == '__main__':
    s = Source()
    
    t = Testbed()
    t.add_component(s, 's')
    t.add_component(Not(), 'n1')
    t.add_component(Not(), 'n2')
    t.add_component(Not(), 'n3')
    t.add_component(Not(), 'n4')
    t.wire('s', 'n1')
    t.wire('n1', 'n2')
    t.wire('n2', 'n3')
    t.wire('n3', 'n4')
    
    s._set_state(True)
    t.update_states()
    
    t.dump_states()
    
    
