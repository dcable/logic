import unittest
import components as c

class AndGateTest(unittest.TestCase):
    """----------
       |0 0 => 0|
       |0 1 => 0|
       |1 0 => 0|
       |1 1 => 1|
       ----------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.source2 = c.Source()
        self.gate = c.And()
        
        self.gate.connect(self.source1, 0)
        self.gate.connect(self.source2, 1)
        
    def test_00(self):
        self.source1.disable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == False

    def test_01(self):
        self.source1.disable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False

    def test_10(self):
        self.source1.enable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_11(self):
        self.source1.enable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == True


class NandGateTest(unittest.TestCase):
    """----------
       |0 0 => 1|
       |0 1 => 1|
       |1 0 => 1|
       |1 1 => 0|
       ----------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.source2 = c.Source()
        self.gate = c.Nand()
        
        self.gate.connect(self.source1, 0)
        self.gate.connect(self.source2, 1)
        
    def test_00(self):
        self.source1.disable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True

    def test_01(self):
        self.source1.disable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == True

    def test_10(self):
        self.source1.enable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_11(self):
        self.source1.enable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
        
class OrGateTest(unittest.TestCase):
    """----------
       |0 0 => 0|
       |0 1 => 1|
       |1 0 => 1|
       |1 1 => 1|
       ----------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.source2 = c.Source()
        self.gate = c.Or()
        
        self.gate.connect(self.source1, 0)
        self.gate.connect(self.source2, 1)
        
    def test_00(self):
        self.source1.disable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_01(self):
        self.source1.disable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_10(self):
        self.source1.enable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_11(self):
        self.source1.enable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == True

class NorGateTest(unittest.TestCase):
    """----------
       |0 0 => 1|
       |0 1 => 0|
       |1 0 => 0|
       |1 1 => 0|
       ----------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.source2 = c.Source()
        self.gate = c.Nor()
        
        self.gate.connect(self.source1, 0)
        self.gate.connect(self.source2, 1)
        
    def test_00(self):
        self.source1.disable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_01(self):
        self.source1.disable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_10(self):
        self.source1.enable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_11(self):
        self.source1.enable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
class XorGateTest(unittest.TestCase):
    """----------
       |0 0 => 0|
       |0 1 => 1|
       |1 0 => 1|
       |1 1 => 0|
       ----------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.source2 = c.Source()
        self.gate = c.Xor()
        
        self.gate.connect(self.source1, 0)
        self.gate.connect(self.source2, 1)
        
    def test_00(self):
        self.source1.disable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_01(self):
        self.source1.disable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_10(self):
        self.source1.enable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_11(self):
        self.source1.enable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False

class XnorGateTest(unittest.TestCase):
    """----------
       |0 0 => 1|
       |0 1 => 0|
       |1 0 => 0|
       |1 1 => 1|
       ----------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.source2 = c.Source()
        self.gate = c.Xnor()
        
        self.gate.connect(self.source1, 0)
        self.gate.connect(self.source2, 1)
        
    def test_00(self):
        self.source1.disable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_01(self):
        self.source1.disable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_10(self):
        self.source1.enable()
        self.source2.disable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
    def test_11(self):
        self.source1.enable()
        self.source2.enable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
class NotGateTest(unittest.TestCase):
    """--------
       |1 => 0|
       |0 => 1|
       --------
    """
    def setUp(self):
        self.source1 = c.Source()
        self.gate = c.Not()
        
        self.gate.connect(self.source1)
        
    def test_0(self):
        self.source1.disable()
        self.gate._op()
        
        assert self.gate.get_state() == True
        
    def test_1(self):
        self.source1.enable()
        self.gate._op()
        
        assert self.gate.get_state() == False
        
        
        
if __name__ == '__main__':
    
    # Test cases to run
    cases = [AndGateTest,
             NandGateTest,
             OrGateTest,
             NorGateTest,
             XorGateTest,
             XnorGateTest,
             NotGateTest,
            ]
    for case in cases:
        suite = unittest.TestLoader().loadTestsFromTestCase(case)
        unittest.TextTestRunner(verbosity=2).run(suite)
